dune-pdelab (2.7~20200605-2) unstable; urgency=medium

  * Upload to unstable.
  * Increase timeout for `recipe-operator-splitting` as it timed on on
    armel and mips64el.
    + Updated patch: increase-timeout.patch

 -- Ansgar <ansgar@debian.org>  Wed, 15 Jul 2020 12:45:47 +0200

dune-pdelab (2.7~20200605-1) experimental; urgency=medium

  * New upstream snapshot.
  * d/control: Add runtime dependency on libeigen3-dev.
    See https://salsa.debian.org/science-team/dune-pdelab/-/merge_requests/1
  * Use debhelper compat level 13.
  * Bumped Standards-Version to 4.5.0 (no changes).

 -- Ansgar <ansgar@debian.org>  Wed, 10 Jun 2020 14:29:46 +0200

dune-pdelab (2.6~20180302-2) unstable; urgency=medium

  * d/control: Build-Depend on libalberta-dev instead of libalberta2-dev.
  * Bumped Standards-Version to 4.4.1.

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 23 Oct 2019 19:13:24 +0200

dune-pdelab (2.6~20180302-1) unstable; urgency=medium

  * New upstream snapshot (commit: e88a57a22a0d42084255eeaa38ac716ab98be96a)
  * d/control: add `Rules-Requires-Root: no`
  * d/control: add explicit (build-)dep on libdune-common-dev
  * libdune-pdelab-doc: add Built-Using: doxygen (for jquery.js)
  * Bumped Standards-Version to 4.1.4 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 13 Apr 2018 08:47:01 +0200

dune-pdelab (2.5.0~rc1-2) unstable; urgency=medium

  * Upload to unstable.
  * Build-Depend on texlive-pictures instead of pgf. (Closes: #867080)
  * debian/copyright: Update URLs.
  * Bumped Standards-Version to 4.0.0 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 18 Jul 2017 12:33:14 +0200

dune-pdelab (2.5.0~rc1-1) experimental; urgency=medium

  * New upstream release candidate.
  * Now Depends: libdune-functions-dev (instead of only Recommends:).

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 24 Feb 2017 10:54:37 +0100

dune-pdelab (2.5.0~20170124g7cf9f47a-1) unstable; urgency=medium

  * New upstream snapshot.
  * Remove 0001-Increase-timeout-to-default-300s.patch (applied upstream).
  * Add Build-Depends: & Recommends: libdune-functions-dev.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 24 Jan 2017 21:51:15 +0100

dune-pdelab (2.5.0~20161204gdb53a76-3) unstable; urgency=medium

  * Increase timeouts for more tests. Hopefully this is enough now for all
    architectures.
    + new patch: increase-timeout.patch

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 07 Dec 2016 22:36:38 +0100

dune-pdelab (2.5.0~20161204gdb53a76-2) unstable; urgency=medium

  * Increase test timeout to 300s. This should fix the build failure on
    several slower architectures.
    + new patch: 0001-Increase-timeout-to-default-300s.patch

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 06 Dec 2016 20:01:51 +0100

dune-pdelab (2.5.0~20161204gdb53a76-1) unstable; urgency=medium

  * New upstream snapshot.
    + Fixes build failure with newer SuperLU versions. (Closes: #838327)
  * Switch to CMake.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 06 Dec 2016 02:14:16 +0100

dune-pdelab (2.4.1-1) unstable; urgency=medium

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 10 Mar 2016 10:02:21 +0100

dune-pdelab (2.4~20160225g3614fe8-1) unstable; urgency=medium

  * New upstream snapshot.
  * Mark libdune-pdelab-dev as Multi-Arch: same.
  * debian/control: Update Vcs-* fields.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 29 Feb 2016 15:13:17 +0100

dune-pdelab (2.4~20150924g056aa26-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 26 Sep 2015 00:51:00 +0200

dune-pdelab (2.4~20150909g1341c52-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 13 Sep 2015 16:09:06 +0200

dune-pdelab (2.4~20150901g19e487b-1) experimental; urgency=medium

  * New upstream snapshot.
  * Move shared library into -dev package and provide a virtual package
    that changes with the upstream version for shlib dependencies. See
    also https://lists.debian.org/debian-devel/2015/07/msg00115.html
  * libdune-pdelab-dev: Add -doc package as a suggested package.
  * Add Build-Depends-Indep: graphviz for "dot".
  * Add build-dep on libeigen3-dev to enable additional tests.
  * Bumped Standards-Version to 3.9.6 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 07 Sep 2015 13:27:49 +0200

dune-pdelab (2.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Change shared library package name to libdune-pdelab-2.0.0.

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 25 Jun 2014 20:10:54 +0200

dune-pdelab (2.0.0~rc2-1) experimental; urgency=medium

  * New upstream release candidate.
  * Change shared library package name to libdune-pdelab-2.0.0rc2.
  * Add libsuperlu-dev as a preferred alternative for libsuperlu3-dev.
    (Closes: #740285)

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 20 Jun 2014 19:48:00 +0200

dune-pdelab (2.0~20140228geb4fffc-1) experimental; urgency=medium

  * New upstream snapshot.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 28 Feb 2014 10:27:23 +0100

dune-pdelab (2.0~20140205g0cee0ce-1) experimental; urgency=medium

  * New upstream snapshot.
  * Install CMake support files.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 17 Feb 2014 12:32:49 +0100

dune-pdelab (2.0~20140111g795acb4-1) experimental; urgency=low

  * Initial release.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 13 Jan 2014 22:58:16 +0100
